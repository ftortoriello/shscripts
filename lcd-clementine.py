#!/usr/bin/env python3
# Franco Tortoriello 2016

# Version 1.0
# LCDproc client which displays information of the song currently played by
# Clementine.
# Licensed under the GPL3

# Requires lcdproc python package (http://pypi.python.org/pypi/lcdproc)

# TODO: Enable customization of the server settings through parameters (host:port)
# TODO: Modularize the main subroutine
# TODO: Turn this into a daemon ???



try:
  from lcdproc.server import Server
except ImportError:
  print("lcdproc python module not found")
  exit(2)

import sys, dbus, time, unicodedata, argparse


class Connection:
  def __init__(self, time, foreground):
    # DBus connection
    session_bus = dbus.SessionBus()
    
    try:
      player = session_bus.get_object('org.mpris.clementine', '/Player')
    except:
      print("Error: Clementine not running")
      sys.exit(1)
    
    self.interface = dbus.Interface(player,
      dbus_interface='org.freedesktop.MediaPlayer')

    # LCDd init
    lcd = Server("127.0.0.1", port=13666)
    lcd.start_session()
    
    self.screen = lcd.add_screen("Clementine")
    if time:
      self.screen.set_duration(time)
    if foreground:
      self.screen.set_priority('foreground')


class Metadata:
  def __init__(self, iface):
    md = iface.GetMetadata()
    st = iface.GetStatus()
    
    self.tags={}
    
    # When first run the dictionary is empty...
    try:
      self.track = int(md['tracknumber'])
      if self.track<0:
        self.track = 0
      elif self.track>99:
        self.track = 0
    except:
      self.track = 0
    
    try:
      artist = conv_string(md['artist'])
    except:
      artist = ''
    if artist != '':
      self.tags['artist'] = artist

    try:
      album = conv_string(md['album'])
    except:
      album = ''
    if album != '':
      self.tags['album'] = album
    
    try:
      year = str(md['year'])
    except:
      year = ''
    if year != '':
      self.tags['year'] = year
    
    try:
      title = conv_string(md['title'])
    except:
      title = ''
    if title != '':
      self.tags['title'] = title
    
    try:
      self.bitrate = int(md['audio-bitrate'])
    except:
      self.bitrate = 0
    
    status = int(st[0])
    if status==0:
      self.status='play'
    elif status==1:
      self.status='pause'
    elif status==2:
      self.status='stop'


def conv_string(string):
  uni_str = str(string)
  std_ascii = ''.join((c for c in unicodedata.normalize('NFD', uni_str) \
     if unicodedata.category(c) != 'Mn'))
  return str(std_ascii)


def main():
  
  # define cmdline arguments
  parser = argparse.ArgumentParser(
    description="Runs a client \
      for LCDproc which displays information from Clementine")
  parser.add_argument('-t', '--time', type=int,
    help='how much time to display the screen')
  parser.add_argument('-f', '--foreground', action='store_true',
    help='display the screen in the foreground')
  parser.add_argument('-s', '--speed', type=float,
    help='the speed in which the display updates its information (default: 0.1)', default=0.1)
  
  args = parser.parse_args()
  
  con = Connection(args.time, args.foreground)

  meta = Metadata(con.interface)
  tracksym_widget = con.screen.add_string_widget(
    "TrackSym", text='T', x=1, y=2)
  trackn_widget = con.screen.add_string_widget(
    "TrackN", text='0', x=2, y=2)
  songtag_widget = con.screen.add_scroller_widget(
    "SongTag", text='', left=1, right=16, top=1, speed=3)
  status_widget = con.screen.add_icon_widget(
    "Status", x=4, y=2, name='stop')
  bitratetxt1_widget = con.screen.add_string_widget(
    "BitrateTxt1", text='@', x=7, y=2)
  bitrate_widget = con.screen.add_string_widget(
    "Bitrate", text='0', x=8, y=2)
  bitratetxt2_widget = con.screen.add_string_widget(
    "BitrateTxt2", text="kbps", x=13, y=2)
  
  cycle_tag=0
  cycle_interval=8
  cycle_current=0
  
  tags_order=['title', 'artist', 'album']
  
  # Update info
  while True:
    # Get updated metadata
    try:
      meta = Metadata(con.interface)
    except dbus.exceptions.DBusException:
      # Clementine is closed, keep running in case its started again
      #songtag_widget.set_text('Not running.')
      print("Clementine has been closed. Terminating now.")
      sys.exit(1)
          
    if cycle_current < cycle_interval:
      cycle_current+=1
    else:
      cycle_current=0
      if cycle_tag<(len(tags_order)-1):
        cycle_tag+=1
      else:
        cycle_tag=0
    
    # Cycle through tags
    tried = 0
    Found = False
    if meta.status != 'stop':
      while (not Found):
        if tags_order[cycle_tag] in meta.tags:
          if (tags_order[cycle_tag] == 'album') and ('year' in meta.tags):
            # Add year to album
            songtag_widget.set_text(meta.tags[tags_order[cycle_tag]] +
              ' (' + meta.tags['year'] + ')')
          else:
            songtag_widget.set_text(meta.tags[tags_order[cycle_tag]])
          Found = True
        else:
          if cycle_tag<(len(tags_order)-1):
            cycle_tag+=1
          else:
            cycle_tag=0
          tried+=1
          if (tried>=len(tags_order)):
            songtag_widget.set_text("No info available")
            break
      
      trackn_widget.set_text(meta.track)
      bitrate_widget.set_text(meta.bitrate)

    else:
      songtag_widget.set_text('Stopped.')
      trackn_widget.set_text(0)
      bitrate_widget.set_text(0)

    status_widget.set_name(meta.status)
      
    

    # Wait...
    time.sleep(args.speed)



if __name__ == "__main__":
  try:
    main()
  except KeyboardInterrupt:
    sys.exit(1);        # Exit cleanly on a ctrl-c.

